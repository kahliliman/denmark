import axios from "axios";

const login = (email, password) =>
  new Promise((resolve, reject) =>
    axios
      .post(`${process.env.REACT_APP_API_URL}/auth/login`, { email, password })
      .then((data) => resolve(data.data))
      .catch(({ response }) => reject(response))
  );

export default login;
