import Cookies from "universal-cookie";

const logout = () => {
  const cookies = new Cookies();
  cookies.set("token", "", { path: "/" });
};

export default logout;
