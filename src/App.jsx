import axios from "axios";
import {
  Route,
  Switch,
  BrowserRouter as Router,
  Redirect,
} from "react-router-dom";
import ROUTES from "./configs/routes";
import LandingPage from "./pages/LandingPage";
import LoginPage from "./pages/LoginPage";
import SignUpPage from "./pages/SignUpPage";
import RoomPage from "./pages/RoomPage";
import NotFoundpage from "./pages/NotFoundpage";
import PrivateRoute from "./components/commons/PrivateRoute";

import "./App.scss";

const App = () => {
  return (
    <Router>
      <Switch>
        <Route path={ROUTES.ROOT} exact>
          <LandingPage />
        </Route>
        <Route path={ROUTES.LOGIN} exact>
          <LoginPage />
        </Route>
        <Route path={ROUTES.SIGNUP} exact>
          <SignUpPage />
        </Route>
        <PrivateRoute path={ROUTES.ROOM} exact>
          <RoomPage />
        </PrivateRoute>
        <Route path="*">
          <NotFoundpage />
        </Route>
      </Switch>
    </Router>
  );
};

export default App;
