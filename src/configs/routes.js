const ROUTES = {
  ROOT: "/",
  LOGIN: "/auth/login",
  SIGNUP: "/auth/register",
  ROOM: "/room",
  NOTFOUND: "/404",
};

export default ROUTES;
