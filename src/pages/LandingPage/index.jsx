import "./LandingPage.scss";
import NavLanding from "../../components/commons/NavLanding";
import SectionIntro from "../../components/resources/SectionIntro";
import SectionGame from "../../components/resources/SectionGame";
import SectionFeatures from "../../components/resources/SectionFeatures";
import SectionRequirements from "../../components/resources/SectionRequirements";
import SectionTopScores from "../../components/resources/SectionTopScores";
import FooterLanding from "../../components/commons/FooterLanding";
import SectionNewsletter from "../../components/resources/SectionNewsletter";
import { useState, useEffect } from "react";
import Cookies from "universal-cookie";
import logout from "../../services/logout";

const LandingPage = () => {
  const cookies = new Cookies();
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    if (cookies.get("token")) {
      setIsLoggedIn(true);
    }
  }, []);

  const logOutHandler = () => {
    logout();
    setIsLoggedIn(false);
  };

  return (
    <div>
      <NavLanding isLoggedIn={isLoggedIn} logOutHandler={logOutHandler} />
      <SectionIntro />
      <SectionGame />
      <SectionFeatures />
      <SectionRequirements />
      <SectionTopScores />
      <SectionNewsletter />
      <FooterLanding />
    </div>
  );
};

export default LandingPage;
