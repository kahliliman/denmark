import NotFound from "../../components/commons/NotFound";

const Notfoundpage = () => {
  return (
    <div>
      <NotFound />
    </div>
  );
};

export default Notfoundpage;
