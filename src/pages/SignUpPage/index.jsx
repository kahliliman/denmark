import SignUp from "../../components/resources/SignUp";
import Nav from "../../components/commons/Nav";

const SignUpPage = () => {
  return (
    <div>
      <Nav />
      <SignUp />
    </div>
  );
};

export default SignUpPage;
