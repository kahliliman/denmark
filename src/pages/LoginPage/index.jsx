import LoginCard from "../../components/resources/LoginCard";
import Nav from "../../components/commons/Nav";

const LoginPage = () => {
  return (
    <div>
      <Nav />
      <LoginCard />
    </div>
  );
};

export default LoginPage;
