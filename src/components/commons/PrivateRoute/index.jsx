import ROUTES from "../../../configs/routes";
import Cookies from "universal-cookie";
import { Redirect } from "react-router-dom";

const PrivateRoute = ({ children }) => {
  const cookies = new Cookies();
  if (!cookies.get("token")) return <Redirect to={ROUTES.LOGIN} />;

  return children;
};

export default PrivateRoute;
