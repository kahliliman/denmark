import "./NavLanding.scss";
import logout from "../../../services/logout";

const NavLanding = (props) => {
  return (
    <header>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark bg-custom">
        <div className="container">
          <a className="navbar-brand" href="#home">
            LOGO
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNavDropdown"
          >
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="collapse navbar-collapse" id="navbarNavDropdown">
            <ul className="navbar-nav mx-auto">
              <li className="nav-item">
                <a className="nav-link custom-link" href="#intro">
                  MAIN
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link custom-link" href="#game">
                  ABOUT
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link custom-link" href="#feature">
                  GAME FEATURES
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link custom-link" href="#sysreq">
                  SYSTEM REQUIREMENTS
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link custom-link" href="#topscore">
                  QUOTES
                </a>
              </li>
            </ul>
            <ul className="navbar-nav">
              <li className={!props.isLoggedIn ? "hidden" : "" + "nav-item"}>
                <a
                  href={process.env.REACT_APP_ROOT_URL + "/profile"}
                  className="nav-link"
                >
                  Profile
                </a>
              </li>
              <li className={!props.isLoggedIn ? "hidden" : "" + "nav-item"}>
                <button onClick={props.logOutHandler}>Log Out</button>
              </li>
              <li className={props.isLoggedIn ? "hidden" : "" + "nav-item"}>
                <a
                  href={process.env.REACT_APP_ROOT_URL + "/auth/register"}
                  className="nav-link signup-link"
                >
                  SIGN UP
                </a>
              </li>
              <li className={props.isLoggedIn ? "hidden" : "" + "nav-item"}>
                <a
                  href={process.env.REACT_APP_ROOT_URL + "/auth/login"}
                  className="nav-link custom-link"
                >
                  LOG IN
                </a>
              </li>

              {/* <li className="nav-item"><a href={process.env.REACT_APP_ROOT_URL + "/profile"} className="nav-link custom-link">
              </a>
              </li> */
              /* <li className="nav-item"><a href={process.env.REACT_APP_ROOT_URL + "/auth/logout"} className="nav-link custom-link"
                id="logout">LOG OUT</a>
              </li> */}
            </ul>
          </div>
        </div>
      </nav>
    </header>
  );
};

export default NavLanding;
