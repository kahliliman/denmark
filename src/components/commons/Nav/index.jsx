import "./Nav.scss";

const Nav = () => {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark  justify-content-end navbar-bg-dark">
      <a className="navbar-brand" href={process.env.REACT_APP_ROOT_URL}>
        <svg
          width="2em"
          height="2em"
          viewBox="0 0 16 16"
          className="bi bi-arrow-left-short"
          fill="currentColor"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            d="M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5z"
          />
        </svg>
      </a>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNav"
        aria-controls="navbarNav"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className="nav-item active">
            <a className="nav-link" href="#home">
              HOME
            </a>
          </li>
        </ul>
        <ul className="navbar-nav ml-auto">
          <li className="nav-item">
            <a
              className="nav-link"
              href={process.env.REACT_APP_ROOT_URL + "/auth/login"}
            >
              Log In
            </a>
          </li>
          <li className="nav-item">
            <a
              className="nav-link"
              href={process.env.REACT_APP_ROOT_URL + "/auth/register"}
            >
              Sign Up
            </a>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Nav;
