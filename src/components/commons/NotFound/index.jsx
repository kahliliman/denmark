import React from "react";
import "./404.scss";

const NotFound = () => {
  return (
    <div id="notfound">
      <div class="notfound">
        <div class="notfound-404">
          <h1>
            4<span>0</span>4
          </h1>
        </div>
        <br></br>
        <br></br>
        <br></br>
        <p>
          The page you are looking for might have been removed had its name
          changed or is temporarily unavailable.
        </p>
        <a href={process.env.REACT_APP_ROOT_URL + "/"}>home page</a>
      </div>
    </div>
  );
};

export default NotFound;
