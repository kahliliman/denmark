import "./SectionTopScores.scss";
import evanImg from "../../../assets/landing/evan.jpg";
import jadaImg from "../../../assets/landing/jada.jpg";
import aaronImg from "../../../assets/landing/aaron.jpg";
import TweetCard from "../TweetCard";

const SectionTopScores = () => {
  return (
    <div className="topscore" id="topscore">
      <div className="topscore__box ">
        <h2 className="section__header">TOP SCORES</h2>
        <p className="section__caption">
          The top score from various games are provided on this platform
        </p>
        <a className="btn btn-lg topscore__btn" href="#topscore" role="button">
          See More
        </a>
      </div>

      <div className="topscore__content">
        <TweetCard
          imgUrl={evanImg}
          title="EVAN LAHTI"
          subtitle="Fish in the Sea"
          description="One of my gaming highlights of the year."
          date="October 18, 2018"
        />

        <TweetCard
          imgUrl={jadaImg}
          title="JADA GRIFFIN"
          subtitle="Nerd Reactor"
          description="The next big thing in the world of streaming and survival games."
          date="December 21, 2018"
        />

        <TweetCard
          imgUrl={aaronImg}
          title="AARON WILLIAMS"
          subtitle="Uproxx"
          description="Snoop Dogg playing the wildly entertaining 'SOS' is ridiculous."
          date="December 24, 2018"
        />
      </div>
    </div>
  );
};

export default SectionTopScores;
