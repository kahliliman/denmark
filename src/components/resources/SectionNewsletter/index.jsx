import "./SectionNewsletter.scss";

const SectionNewsletter = () => {
  return (
    <div className="newsletter" id="newsletter">
      <div className="newsletter__box">
        <p className="section__caption">Want to stay in touch?</p>
        <h2 className="section__header">NEWSLETTER SUBSCRIBE</h2>
        <p className="section__caption-2">
          In order to start receiving our news, all you have to do is enter your
          email address. Everything else will be taken care of by us. We will
          send you emails containing informations about our games. We don't
          spam.
        </p>

        <form className="form-inline">
          <label className="sr-only" htmlFor="inlineFormInputEmail">
            Email
          </label>
          <input
            type="email"
            className="form-control form-control-lg mb-2 mr-sm-2 newsletter__form"
            id="inlineFormInputEmail"
            placeholder="youremail@binar.co.id"
          />

          <button type="submit" className="btn mb-2 newsletter__btn">
            Subscribe Now
          </button>
        </form>
      </div>
    </div>
  );
};

export default SectionNewsletter;
