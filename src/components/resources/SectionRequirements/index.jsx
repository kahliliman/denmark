import "./SectionRequirements.scss";

const SectionRequirements = () => {
  return (
    <div className="sysreq" id="sysreq">
      <div className="sysreq__box">
        <div className="sysreq__header">
          <p className="section__caption">Can my computer run this game?</p>
          <h2 className="section__header">SYSTEM REQUIREMENTS</h2>
        </div>
        <table className="">
          <tbody>
            <tr>
              <td>
                <h3 className="section__header-2">OS:</h3>
                <p className="section__caption-2">
                  Windows 7 64-bit only (No OSX support at this time
                </p>
              </td>
              <td>
                <h3 className="section__header-2">PROCESSOR:</h3>
                <p className="section__caption-2">
                  Intel Core 2 Duo @ 2.4 GHz or AMD Athlon X2 @ 2.8 GHz
                </p>
              </td>
            </tr>
            <tr>
              <td>
                <h3 className="section__header-2">MEMORY:</h3>
                <p className="section__caption-2">4 GB RAM</p>
              </td>
              <td>
                <h3 className="section__header-2">STORAGE:</h3>
                <p className="section__caption-2">8 GB available space</p>
              </td>
            </tr>
            <tr>
              <td colSpan="2">
                <h3 className="section__header-2">GRAPHICS:</h3>
                <p className="section__caption-2">
                  Nvidia GeForce GTX 660 2GB or AMD Radeon HD 7850 2GB DirectX11
                  (Shader Model 5)
                </p>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default SectionRequirements;
