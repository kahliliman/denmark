import "./SectionGame.scss";
import featureImage from "../../../assets/landing/rockpaperstrategy-1600.jpg";

const SectionGame = () => {
  return (
    <div className="game" id="game">
      <p className="section__caption">What's so special?</p>
      <div className="d-flex justify-content-between flex-wrap">
        <div className="game__textbox">
          <h2 className="section__header">THE GAMES</h2>
        </div>
        <div
          id="carouselControls"
          className="game__content carousel slide"
          data-ride="carousel"
        >
          <ol className="carousel-indicators">
            <li
              data-target="#carouselControls"
              data-slide-to="0"
              className="active"
            ></li>
            <li data-target="#carouselControls" data-slide-to="1"></li>
            <li data-target="#carouselControls" data-slide-to="2"></li>
          </ol>
          <div className="carousel-inner">
            <div className="carousel-item active">
              <img src={featureImage} className="d-block" alt="..." />
            </div>
            <div className="carousel-item">
              <img src={featureImage} className="d-block" alt="..." />
            </div>
            <div className="carousel-item">
              <img src={featureImage} className="d-block" alt="..." />
            </div>
          </div>
          <a
            className="carousel-control-prev"
            href="#carouselControls"
            role="button"
            data-slide="prev"
          >
            <span
              className="carousel-control-prev-icon"
              aria-hidden="true"
            ></span>
            <span className="sr-only">Previous</span>
          </a>
          <a
            className="carousel-control-next"
            href="#carouselControls"
            role="button"
            data-slide="next"
          >
            <span
              className="carousel-control-next-icon"
              aria-hidden="true"
            ></span>
            <span className="sr-only">Next</span>
          </a>
        </div>
      </div>
    </div>
  );
};

export default SectionGame;
