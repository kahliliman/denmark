import "./TweetCard.scss";

const TweetCard = (props) => {
  return (
    <div className="tweet-card">
      <div className="tweet-card__header">
        <div className="tweet-card__avatar">
          <div className="circle">
            <div className="thumb">
              <img className="avatar__image" src={props.imgUrl} alt="img" />
            </div>
          </div>
          <div className="tweet-card__name">
            <div className="tweet-card__title">{props.title}</div>
            <div className="tweet-card__subtitle">{props.subtitle}</div>
          </div>
        </div>
        <div className="tweet-card__twitter"></div>
      </div>
      <div className="tweet-card__content">
        <p className="tweet-card__comment">"{props.description}"</p>
        <p className="tweet-card__date">{props.date}</p>
      </div>
    </div>
  );
};

export default TweetCard;
