import "./SectionFeatures.scss";

const SectionFeatures = () => {
  return (
    <div className="feature" id="feature">
      <div className="feature__box">
        <div className="feature__header">
          <p className="section__caption">What's so special?</p>
          <h2 className="section__header">FEATURES</h2>
        </div>
        <div className="feature__content">
          <h3 className="section__header-2">TRADITIONAL GAMES</h3>
          <p className="section__caption-2">
            If you miss your childhood, we provide many traditional games here
          </p>
        </div>
        <div className="feature__content">
          <h3 className="section__header-2">GAME SUIT</h3>
        </div>
        <div className="feature__content">
          <h3 className="section__header-2">TBD</h3>
        </div>
      </div>
    </div>
  );
};

export default SectionFeatures;
