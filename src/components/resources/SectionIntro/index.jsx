import "./SectionIntro.scss";

const SectionIntro = () => {
  return (
    <div className="intro" id="intro">
      <div className="jumbotron">
        <h1 className="jumbotron__title">PLAY TRADITIONAL GAME</h1>
        <p className="jumbotron__caption">
          Experience new traditional gameplay
        </p>
        <a
          className="btn btn-lg jumbotron__btn"
          href={process.env.REACT_APP_ROOT_URL + "/room"}
          role="button"
        >
          PLAY NOW
        </a>
      </div>

      <div className="intro__caption">
        <p className="intro__caption__text">The Story</p>
      </div>
    </div>
  );
};

export default SectionIntro;
