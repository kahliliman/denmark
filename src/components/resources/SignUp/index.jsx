import React from "react";
import loginImg from "../../../assets/gaming.svg";
import "./Signup.scss";

const SignUp = () => {
  return (
    <div className="base-container">
      <div className="header">Sign Up</div>
      <div className="content">
        <div className="image">
          <img src={loginImg} />
        </div>
        <div className="form">
          <div className="form-group">
            <label htmlFor="username">Full Name</label>
            <input type="text" name="fullname" placeholder="fullname" />
          </div>
          <div className="form-group">
            <label htmlFor="username">Username</label>
            <input type="text" name="username" placeholder="username" />
          </div>
          <div className="form-group">
            <label htmlFor="username">Email Address</label>
            <input type="text" name="email" placeholder="email" />
          </div>
          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input type="password" name="password" placeholder="password" />
          </div>
        </div>
      </div>
      <div className="footer">
        <button type="button" className="btn-lgn">
          Login
        </button>
      </div>
    </div>
  );
};

export default SignUp;
