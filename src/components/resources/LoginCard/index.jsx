import ROUTES from "../../../configs/routes";
import React from "react";
import loginApi from "../../../services/login";
import { useState } from "react";
import { useHistory, Redirect } from "react-router-dom";
import loginImg from "../../../assets/gaming.svg";
import Cookies from "universal-cookie";
import "./Login.scss";

const LoginCard = () => {
  const cookies = new Cookies();
  const history = useHistory();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isError, setIsError] = useState(false);

  const onSubmit = () =>
    loginApi(email, password)
      .then(({ token }) => {
        cookies.set("token", token, { path: "/" });

        history.push(ROUTES.ROOT);
      })
      .catch((e) => {
        if (e.status === 401) {
          setIsError(true);
        }
      });

  if (cookies.get("token")) return <Redirect to={ROUTES.ROOT} />;

  return (
    <div className="base-container">
      <div className="header">Login</div>
      <div className="content">
        <div className="image">
          <img src={loginImg} alt="..." />
        </div>
        <div className="form">
          <div className="form-group">
            <label htmlFor="email">Email</label>
            <input
              className={`form-control ${isError ? "is-invalid" : ""}`}
              id="email"
              type="email"
              placeholder="email"
              value={email}
              onChange={(e) => {
                setIsError(false);
                setEmail(e.target.value);
              }}
            />
          </div>

          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input
              className={`form-control ${isError ? "is-invalid" : ""}`}
              id="password"
              type="password"
              placeholder="password"
              value={password}
              onChange={(e) => {
                setIsError(false);
                setPassword(e.target.value);
              }}
            />
          </div>
        </div>
      </div>
      <div className="footer">
        <button type="button" className="btn-lgn" onClick={onSubmit}>
          Login
        </button>
      </div>
    </div>
  );
};

export default LoginCard;
